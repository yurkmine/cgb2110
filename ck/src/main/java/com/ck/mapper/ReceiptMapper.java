package com.ck.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ck.pojo.Receipt;
import org.apache.ibatis.annotations.Update;

public interface ReceiptMapper extends BaseMapper<Receipt> {
    Receipt findReceipt(Integer id);
    @Update("update receipt set join_price=#{joinPrice},out_Price=#{outPrice},num=#{num},updated=#{updated} where id=#{id}")
    void updateReceipt(Receipt receipt);
}
