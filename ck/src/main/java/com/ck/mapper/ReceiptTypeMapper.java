package com.ck.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ck.pojo.ReceiptType;

public interface ReceiptTypeMapper extends BaseMapper<ReceiptType>{

}
