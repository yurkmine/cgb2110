package com.ck.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ck.pojo.CompanyType;

public interface CompanyTypeMapper extends BaseMapper<CompanyType> {
}
