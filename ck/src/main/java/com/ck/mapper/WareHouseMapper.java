package com.ck.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ck.pojo.WareHouseType;

import java.util.List;

public interface WareHouseMapper extends BaseMapper<WareHouseType> {

    WareHouseType findWarehouse(Integer id);
}
