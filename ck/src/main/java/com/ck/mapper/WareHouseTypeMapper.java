package com.ck.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ck.pojo.WareHouseType;

public interface WareHouseTypeMapper extends BaseMapper<WareHouseType> {
}
