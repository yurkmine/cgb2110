package com.ck.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ck.pojo.Repertory;

public interface RepertoryMapper extends BaseMapper<Repertory> {
}
