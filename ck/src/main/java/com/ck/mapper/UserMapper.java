package com.ck.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ck.pojo.User;

public interface UserMapper extends BaseMapper<User> {
    User login(User user);

    void login2(User user);
}
