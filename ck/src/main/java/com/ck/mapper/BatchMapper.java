package com.ck.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ck.pojo.Receipt;

public interface BatchMapper extends BaseMapper<Receipt> {
}
