package com.ck.controller;

import com.ck.pojo.Receipt;
import com.ck.service.BatchService;
import com.ck.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/batch")
public class BatchController {
    @Autowired
    private BatchService batchService;
    @PostMapping("/addByBatch")
    public SysResult addByBatch(@RequestBody List<Receipt> arr){
        for(Receipt receipt:arr) {
            batchService.addByBatch(receipt);
        }
        return SysResult.success();
    }
}
