package com.ck.controller;

import com.ck.pojo.Repertory;
import com.ck.service.RepertoryService;
import com.ck.vo.PageResult;
import com.ck.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/repertory")
public class RepertoryController {
    @Autowired
    private RepertoryService repertoryService;
    @GetMapping("/getRepertoryList")
    public SysResult getRepertoryList(PageResult pageResult){
        pageResult=repertoryService.getRepertoryList(pageResult);
        return SysResult.success(pageResult);
    }
}
