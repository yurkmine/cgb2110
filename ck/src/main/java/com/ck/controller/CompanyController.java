package com.ck.controller;

import com.ck.service.CompanyService;
import com.ck.vo.PageResult;
import com.ck.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/company")
public class CompanyController {
    @Autowired
    private CompanyService companyService;
    @GetMapping("/getCompanyList")
    public SysResult getCompanyList(PageResult pageResult){
        pageResult=companyService.getCompanyList(pageResult);
        return SysResult.success(pageResult);
    }
}
