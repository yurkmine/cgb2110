package com.ck.controller;

import com.ck.pojo.WareHouseType;
import com.ck.service.WareHouseTypeService;
import com.ck.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/warehouseType")
public class WareHouseTypeController {
    @Autowired
    private WareHouseTypeService wareHouseTypeService;
    @GetMapping("/getWareHouse/{rank}")
    public SysResult getWareHouse(@PathVariable Integer rank){
        List<WareHouseType> wareHouseTypeList=wareHouseTypeService.getWareHouse(rank);
        return SysResult.success(wareHouseTypeList);
    }
}
