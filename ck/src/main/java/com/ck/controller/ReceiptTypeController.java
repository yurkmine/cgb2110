package com.ck.controller;

import com.ck.pojo.Receipt;
import com.ck.pojo.ReceiptType;
import com.ck.service.ReceiptTypeService;
import com.ck.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/receiptType")
public class ReceiptTypeController {
    @Autowired
    private ReceiptTypeService receiptTypeService;
    @GetMapping("getReceiptType/{rank}")
    public SysResult getReceiptType(@PathVariable Integer rank){
        List<ReceiptType> receiptTypeList=receiptTypeService.getReceiptType(rank);
        return SysResult.success(receiptTypeList);
    }
}
