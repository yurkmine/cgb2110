package com.ck.controller;

import com.ck.pojo.User;
import com.ck.service.UserService;
import com.ck.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;
    @PostMapping("/login")
    public SysResult login(@RequestBody User user){
        String token=userService.login(user);//密钥
        System.out.println(token);
        if(token==null){//返回密钥为空
            return SysResult.fail();
        }
        return SysResult.success(token+user.getUsername());
    }
    @PostMapping("/login2")
    public SysResult login2(@RequestBody User user){
        userService.login2(user);//密钥
        return SysResult.success(user.getUsername());
    }
}
