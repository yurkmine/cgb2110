package com.ck.controller;

import com.ck.pojo.CompanyType;
import com.ck.service.CompanyTypeService;
import com.ck.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/companyType")
public class CompanyTypeController {
    @Autowired
    private CompanyTypeService companyTypeService;
    @GetMapping("/getCompanyType/{rank}")
    public SysResult getCompanyType(@PathVariable Integer rank){
        List<CompanyType> companyTypeList=companyTypeService.getCompanyType(rank);
        return SysResult.success(companyTypeList);
    }
}
