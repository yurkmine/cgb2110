package com.ck.controller;

import com.ck.pojo.Receipt;
import com.ck.service.ReceiptService;
import com.ck.vo.PageResult;
import com.ck.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@CrossOrigin
@RequestMapping("/receipt")
public class ReceiptController {
    @Autowired
    private ReceiptService receiptService;
    @GetMapping("/getReceiptList")
    public SysResult getReceiptList( PageResult pageResult){
         pageResult=receiptService.getReceiptList(pageResult);
         return SysResult.success(pageResult);
    }
    @PostMapping("/addReceipt")
    public SysResult addReceipt(@RequestBody Receipt receipt){
        receiptService.addReceipt(receipt);
        return SysResult.success();
    }
    @DeleteMapping("/{id}")
    public SysResult deleteRow(@PathVariable Integer id){
        receiptService.deleteRow(id);
        return SysResult.success();
    }
    @RequestMapping("/deleteReceipt")
    public SysResult deleteReceipt(@RequestBody Map<String,Object> params){
        //获取回传id字符串
        String ids = params.get("ids").toString();
        String[] id = ids.split(",");
        Integer[] arr=new Integer[ids.length()];
        for(int i=0;i<(id.length);i++){
            arr[i]=Integer.parseInt(id[i]);
        }
        receiptService.deleteReceipt(arr);
        return SysResult.success();
    }
    @GetMapping("/{id}")
    public SysResult findReceipt(@PathVariable Integer id){
        Receipt receipt=receiptService.findReceipt(id);
        if(receipt==null){
            return SysResult.fail();
        }
        return SysResult.success(receipt);
    }
    @PutMapping("/updateReceipt")
    public SysResult updateReceipt(@RequestBody Receipt receipt){
        System.out.println(receipt);
        receiptService.updateReceipt(receipt);
        return SysResult.success();
    }
}
