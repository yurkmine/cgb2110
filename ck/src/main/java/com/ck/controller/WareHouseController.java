package com.ck.controller;

import com.ck.pojo.WareHouseType;
import com.ck.service.WareHouseService;
import com.ck.vo.PageResult;
import com.ck.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/warehouse")
public class WareHouseController {
    @Autowired
    private WareHouseService wareHouseService;
    @GetMapping("/getWarehouseList")
    public SysResult getWarehouseList(PageResult pageResult){
        pageResult=wareHouseService.getWarehouseList(pageResult);
        return SysResult.success(pageResult);
    }
    @PostMapping("/addWarehouse")
    public SysResult addWarehouse(@RequestBody WareHouseType wareHouseType){
        wareHouseService.addWarehouse(wareHouseType);
        return SysResult.success();
    }
    @DeleteMapping("/{id}")
    public SysResult deleteRow(@PathVariable("id") Integer id){
        wareHouseService.deleteRow(id);
        return SysResult.success();
    }
    @GetMapping("/{id}")
    public SysResult findWarehouse(@PathVariable("id") Integer id){
        WareHouseType wareHouseType=wareHouseService.findWarehouse(id);
        //判断返回对象是否存在
        if(wareHouseType==null){
            return SysResult.fail();
        }
        return SysResult.success(wareHouseType);
    }
    @PutMapping("/updateWarehouse")
    public SysResult updateWarehouse(@RequestBody WareHouseType wareHouseType){
        wareHouseService.updateWarehouse(wareHouseType);
        return SysResult.success();
    }
}
