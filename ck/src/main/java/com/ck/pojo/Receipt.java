package com.ck.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@Accessors(chain = true)
@TableName("receipt")
public class Receipt extends BasePojo{
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String receiptType;
    private Date date;
    private String goods;
    private double joinPrice;
    private double outPrice;
    private Integer num;
    private String company;
    private double wholeSale;
    private String wareHouse;
}
