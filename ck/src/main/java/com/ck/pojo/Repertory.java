package com.ck.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@TableName("repertory")
public class Repertory{
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String goods;
    private String unit;
    private String specification;
    private String storageLocation;
    private Integer num;
    private double outPrice;
}
