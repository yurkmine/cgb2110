package com.ck.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@TableName("company_type")
public class CompanyType {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String companyType;
    private Integer rank;
}
