package com.ck.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@TableName("warehouse_type")
public class WareHouseType {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String warehouseType;
    private Integer rank;
    private String addr;
}
