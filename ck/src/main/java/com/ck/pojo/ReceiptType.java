package com.ck.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@TableName("receipt_type")
public class ReceiptType{
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String receiptType;
    private Integer rank;
}
