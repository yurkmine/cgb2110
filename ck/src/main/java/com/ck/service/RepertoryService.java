package com.ck.service;

import com.ck.vo.PageResult;

public interface RepertoryService {
    PageResult getRepertoryList(PageResult pageResult);
}
