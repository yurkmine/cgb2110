package com.ck.service;

import com.ck.vo.PageResult;

public interface CompanyService {
    PageResult getCompanyList(PageResult pageResult);
}
