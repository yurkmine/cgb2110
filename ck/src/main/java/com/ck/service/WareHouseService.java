package com.ck.service;

import com.ck.pojo.WareHouseType;
import com.ck.vo.PageResult;

import java.util.List;

public interface WareHouseService {
    PageResult getWarehouseList(PageResult pageResult);

    void addWarehouse(WareHouseType wareHouseType);

    void deleteRow(Integer id);

    WareHouseType findWarehouse(Integer id);

    void updateWarehouse(WareHouseType wareHouseType);
}
