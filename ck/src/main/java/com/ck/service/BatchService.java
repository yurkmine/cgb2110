package com.ck.service;

import com.ck.pojo.Receipt;

public interface BatchService {

    void addByBatch(Receipt receipt);
}
