package com.ck.service;

import com.ck.pojo.WareHouseType;

import java.util.List;

public interface WareHouseTypeService {
    List<WareHouseType> getWareHouse(Integer rank);
}
