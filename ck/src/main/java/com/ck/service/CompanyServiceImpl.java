package com.ck.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ck.mapper.CompanyMapper;
import com.ck.pojo.CompanyType;
import com.ck.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class CompanyServiceImpl implements CompanyService{
    @Autowired
    private CompanyMapper companyMapper;

    @Override
    public PageResult getCompanyList(PageResult pageResult) {
        QueryWrapper<CompanyType> queryWrapper=new QueryWrapper<>();
        boolean flag= StringUtils.hasLength(pageResult.getQuery());
        queryWrapper.like(flag, "companyType", pageResult.getQuery());
        Page<CompanyType> page=new Page<>(pageResult.getPageNum(),pageResult.getPageSize());
        page=companyMapper.selectPage(page,queryWrapper);
        long total = page.getTotal();
        List<CompanyType> rows = page.getRecords();
        return pageResult.setRows(rows).setTotal(total);
    }
}
