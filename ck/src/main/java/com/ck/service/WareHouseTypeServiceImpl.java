package com.ck.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ck.mapper.WareHouseTypeMapper;
import com.ck.pojo.WareHouseType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WareHouseTypeServiceImpl implements WareHouseTypeService{
    @Autowired
    private WareHouseTypeMapper wareHouseTypeMapper;

    @Override
    public List<WareHouseType> getWareHouse(Integer rank) {
        QueryWrapper<WareHouseType> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq("rank", rank);
        List<WareHouseType> wareHouseTypeList = wareHouseTypeMapper.selectList(queryWrapper);
        return wareHouseTypeList;
    }
}
