package com.ck.service;

import com.ck.pojo.CompanyType;

import java.util.List;

public interface CompanyTypeService {

    List<CompanyType> getCompanyType(Integer rank);
}
