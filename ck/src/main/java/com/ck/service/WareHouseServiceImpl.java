package com.ck.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ck.mapper.WareHouseMapper;
import com.ck.pojo.WareHouseType;
import com.ck.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class WareHouseServiceImpl implements WareHouseService{
    @Autowired
    private WareHouseMapper wareHouseMapper;

    @Override
    public PageResult getWarehouseList(PageResult pageResult) {
        QueryWrapper<WareHouseType> queryWrapper=new QueryWrapper<>();
        //查询判断 query是否为null
        boolean flag= StringUtils.hasLength(pageResult.getQuery());
        queryWrapper.like(flag, "warehouse_type", pageResult.getQuery());
        //自动分页需要配置类
        Page<WareHouseType> page=new Page<>(pageResult.getPageNum(),pageResult.getPageSize());
        page=wareHouseMapper.selectPage(page, queryWrapper);
        long total=page.getTotal();
        List<WareHouseType> rows=page.getRecords();
        return pageResult.setTotal(total).setRows(rows);
    }

    @Override
    public void addWarehouse(WareHouseType wareHouseType) {
        wareHouseMapper.insert(wareHouseType);
    }

    @Override
    public void deleteRow(Integer id) {
        wareHouseMapper.deleteById(id);
    }

    @Override
    public WareHouseType findWarehouse(Integer id) {
        return wareHouseMapper.findWarehouse(id);
    }

    @Override
    public void updateWarehouse(WareHouseType wareHouseType) {
        QueryWrapper<WareHouseType> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq("warehouse_type", wareHouseType.getWarehouseType());
        wareHouseMapper.update(wareHouseType.setAddr(wareHouseType.getAddr()),queryWrapper);
    }

}
