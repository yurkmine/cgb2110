package com.ck.service;
import com.ck.pojo.ReceiptType;

import java.util.List;

public interface ReceiptTypeService {


    List<ReceiptType> getReceiptType(Integer rank);
}
