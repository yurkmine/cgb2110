package com.ck.service;

import com.ck.pojo.Receipt;
import com.ck.vo.PageResult;


public interface ReceiptService {

    PageResult getReceiptList(PageResult pageResult);

    void addReceipt(Receipt receipt);

    void deleteRow(Integer id);


    void deleteReceipt(Integer[] arr);

    Receipt findReceipt(Integer id);

    void updateReceipt(Receipt receipt);
}
