package com.ck.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ck.mapper.CompanyTypeMapper;
import com.ck.pojo.CompanyType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyTypeServiceImpl implements CompanyTypeService{
    @Autowired
    private CompanyTypeMapper companyTypeMapper;

    @Override
    public List<CompanyType> getCompanyType(Integer rank) {
        QueryWrapper<CompanyType> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq("rank", rank);
        List<CompanyType> companyTypeList = companyTypeMapper.selectList(queryWrapper);
        return companyTypeList;
    }
}
