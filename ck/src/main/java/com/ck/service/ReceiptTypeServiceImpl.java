package com.ck.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ck.mapper.ReceiptTypeMapper;
import com.ck.pojo.Receipt;
import com.ck.pojo.ReceiptType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReceiptTypeServiceImpl implements ReceiptTypeService{
    @Autowired
    private ReceiptTypeMapper receiptTypeMapper;


    @Override
    public List<ReceiptType> getReceiptType(Integer rank) {
        QueryWrapper<ReceiptType> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq("rank", rank);
        List<ReceiptType> list=receiptTypeMapper.selectList(queryWrapper);
        return list;
    }
}
