package com.ck.service;

import com.ck.mapper.UserMapper;
import com.ck.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.util.UUID;

@Service
public class UserServiceIMpl implements UserService{
    @Autowired
    private UserMapper userMapper;

    @Override
    public String login(User user) {
        /*String password=user.getPassword();
        String md5Pass= DigestUtils.md5DigestAsHex(password.getBytes());
        user.setPassword(md5Pass);*/
        User userDB=userMapper.login(user);
        if(userDB==null){//未查找到数据
            return null;
        }
        String token=UUID.randomUUID().toString().replace("-", "");
        return token;
    }

    @Override
    public void login2(User user) {
        userMapper.login2(user);
    }
}
