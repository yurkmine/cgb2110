package com.ck.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ck.mapper.RepertoryMapper;
import com.ck.pojo.Repertory;
import com.ck.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class RepertoryServiceImpl implements RepertoryService{
    @Autowired
    private RepertoryMapper repertoryMapper;

    @Override
    public PageResult getRepertoryList(PageResult pageResult) {
        QueryWrapper<Repertory> queryWrapper=new QueryWrapper<>();
        boolean flag= StringUtils.hasLength(pageResult.getQuery());
        queryWrapper.like(flag,"goods",pageResult.getQuery());
        Page<Repertory> page=new Page<>(pageResult.getPageNum(),pageResult.getPageNum());
        page=repertoryMapper.selectPage(page, queryWrapper);
        //获取总条数
        long total = page.getTotal();
        //获取分页后的页数
        List<Repertory> rows = page.getRecords();
        return pageResult.setTotal(total).setRows(rows);
    }
}
