package com.ck.service;

import com.ck.mapper.BatchMapper;
import com.ck.pojo.Receipt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BatchServiceImpl implements BatchService{
    @Autowired
    private BatchMapper batchMapper;


    @Override
    public void addByBatch(Receipt receipt) {
        batchMapper.insert(receipt);
    }
}
