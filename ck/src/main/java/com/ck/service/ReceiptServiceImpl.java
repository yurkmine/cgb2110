package com.ck.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ck.mapper.ReceiptMapper;
import com.ck.pojo.Receipt;
import com.ck.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.List;

@Service
public class ReceiptServiceImpl implements ReceiptService{
    @Autowired
    private ReceiptMapper receiptMapper;


    @Override
    @Transactional
    public PageResult getReceiptList(PageResult pageResult) {
        //1.用户查询的where条件
        QueryWrapper<Receipt> queryWrapper=new QueryWrapper<>();
        //判断query是否为null
        boolean flag= StringUtils.hasLength(pageResult.getQuery());
        queryWrapper.like(flag,"id",pageResult.getQuery());
        //2.page是MP分页对象 参数一：页数 参数二：条数
        //实现需要配置类文件
        Page<Receipt> page=
                new Page<>(pageResult.getPageNum(),pageResult.getPageSize());//2个参数
        //MP在内部自动的获取 总数和分页后的结果
        page=receiptMapper.selectPage(page,queryWrapper);//4个参数 2+2操作
        //获取数据之后封装 总数
        long total=page.getTotal();
        List<Receipt> rows=page.getRecords();
        //返回总数和分页后的结果 给到pageResult
        return pageResult.setTotal(total).setRows(rows);
    }

    @Override
    @Transactional
    public void addReceipt(Receipt receipt) {
        Integer a=receipt.getNum();
        double b=receipt.getJoinPrice();
        receipt.setWholeSale(a*b);
        receiptMapper.insert(receipt);
    }

    @Override
    @Transactional
    public void deleteRow(Integer id) {
        receiptMapper.deleteById(id);
    }

    @Override
    public void deleteReceipt(Integer[] arr) {
        List<Integer> list = Arrays.asList(arr);
        receiptMapper.deleteBatchIds(list);
    }

    @Override
    public Receipt findReceipt(Integer id) {
        return receiptMapper.findReceipt(id);
    }

    @Override
    public void updateReceipt(Receipt receipt) {
        receiptMapper.updateReceipt(receipt);
    }
}
