package com.jt.mapper;

import com.jt.pojo.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserMapper {
    List<User> findAll();

    void saveUser(User user);

    void deleteUser(Integer id);

    void updateUser(User user);

    User findUserById(Integer id);

    List<User> findUserByLike(String name);

    List<User> findUserByIn(Integer[] id);


    List<User> findUserByIn2(@Param("minAge") Integer minAge, @Param("maxAge") Integer maxAge);
}
