package com.jt.controller;

import com.jt.pojo.User;
import com.jt.service.UserService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/user")
@ResponseBody
public class UserController {
    @Autowired
    private UserService userService;
    @GetMapping("/findAll")
    public List<User> findAll(){
        return userService.findAll();
    }
    @GetMapping("/{name}/{age}/{sex}")
    public String saveUser(User user){
        userService.saveUser(user);
        return "用户添加成功";
    }
    @GetMapping("/deleteUser")
    public String deleteUser(Integer id){
        userService.deleteUser(id);
        return "用户删除成功";
    }
    @GetMapping("/update/{name}/{age}/{sex}/{id}")
    public String updateUser(User user){
        userService.updateUser(user);
        return "用户更新成功";
    }
    @GetMapping("/findUserById")
    @ResponseBody
    public User findUserById(Integer id){
        return userService.findUserById(id);
    }
    @GetMapping("findUserByLike")
    public List<User> findUserByLike(String name){
        return userService.findUserByLike(name);
    }
    @GetMapping("/findUserByIn")
    public List<User> findUserByIn(Integer[] id){
        return userService.findUserByIn(id);
    }
    @GetMapping("/findUserByIn2")
    public List<User> findUserByIn2(@Param("minAge") Integer minAge, @Param("maxAge") Integer maxAge){
        return userService.findUserByIn2(minAge,maxAge);
    }


}
