package com.jt.demo.UserService;

import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    @Override
    public void addUser() {
        System.out.println("用户添加成功");
    }

    @Override
    public void deleteUser() {
        System.out.println("用户删除成功");
    }
}
