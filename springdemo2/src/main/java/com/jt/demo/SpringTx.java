package com.jt.demo;

import com.jt.demo.UserService.UserService;
import com.jt.demo.config.SpringConfig;
import com.jt.demo.proxy.JDKProxy;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


public class SpringTx {
    public static void main(String[] args) {
        ApplicationContext context=new AnnotationConfigApplicationContext(SpringConfig.class);
        UserService userService = context.getBean(UserService.class);
        UserService proxy=(UserService)JDKProxy.getProxy(userService);
        System.out.println(proxy.getClass());
        proxy.addUser();
        proxy.deleteUser();
        System.out.println(proxy);//打印输出对象 默认调用tostring()方法

    }
}
