package com.jt.demo2.UserService;

public interface UserService {
    void addUser();
    void deleteUser();
    int findCount();//查询总数
}
