package com.jt.demo2.UserService;

import com.jt.demo2.anno.Cgb2110;
import com.jt.demo2.anno.Find;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    @Override
    @Cgb2110
    @Find(id = 101)//使用注解
    public void addUser() {
        System.out.println("用户添加成功");
    }

    @Override

    public void deleteUser() {
        System.out.println("用户删除成功");
    }

    @Override
    @Cgb2110//使用注解 被拦截 测试获取返回值的!!!
    public int findCount() {
        return 1000;
    }
}
