package com.jt.demo2.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Aspect
@Order(2)
public class SpringAOP2 {
    //在AOP1执行过程中执行 proceed()方法 有下一个通知执行下一个通知AOP2，没有再执行目标方法 类似嵌套结构
    @Around("@annotation(com.jt.demo2.anno.Cgb2110)")//不用定义pointcut 直接将定义在pointcut内的内容写入
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        System.out.println("环绕开始二");
        Object result= joinPoint.proceed();
        System.out.println("环绕结束二");
        return result;
    }
}
