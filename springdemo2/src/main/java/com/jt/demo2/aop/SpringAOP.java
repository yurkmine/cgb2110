package com.jt.demo2.aop;

import com.jt.demo2.anno.Find;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.concurrent.Executors;

@Component //将当前类交给Spring容器管理
@Aspect //表示我是一个切面类 AOP
@Order(1) //可以用Order关键字 实现AOP的排序 数字越小越先执行
public class SpringAOP {
    /*
    * 切面 =切入点表达式+通知方法
    * 1.切入点：
    *   理解：可以理解为就是一个if判断
    *   判断条件：切入点表达式
    *   规则：满足表达式 则判断为true，则执行通知方法
    *        不满足表达式 则判断为false 则不执行通知方法
    *
    * 2.切入点表达式
    * 2.1   bean("对象的ID")  每次只拦截(匹配)一个
    * 2.2   within("包名.类名")
    * 2.3   execution(返回值类型 包名.类名.方法名(参数列表))
    * 2.4   @annotation(注解的路径)
    *
    * 3.切入点表达式案例练习
    *   3.1@Pointcut("bean(userServiceImpl)")只匹配ID为userServiceImpl的对象
    *   3.2@Pointcut("within(com.jt.demo2.UserService.*)")匹配xx.xx.UserService下的所有对象
    *   说明：上述的操作，匹配粒度都是粗粒度的，按类匹配
    *   3.3@Pointcut("execution(* com.jt.demo2.UserService..*.*(..))")
    *   ".."任意个数的任意参数类型 "UserService..*"UserService下所有包包括子孙包下的所有类的任意方法
    *   @Pointcut("execution(* com.jt.demo2.UserService..*.add*(..))")
    *   ".."任意个数的任意参数类型 "UserService..*"UserService下所有包包括子孙包下的所有类，以add开头的方法
    *   3.4@Pointcut("@annotation(com.jt.demo2.anno.Cgb2110)")
    *   拦截xx包下的Cgb2110的注解
    * */

    //@Pointcut("bean(userServiceImpl)")
    //@Pointcut("within(com.jt.demo2.UserService.*)")
    //@Pointcut("execution(* com.jt.demo2.UserService..*.add*(..))")
    @Pointcut("@annotation(com.jt.demo2.anno.Cgb2110)")
    //拦截用户类对象 与切入点表达式匹配 为了后续拓展方便 为其自动内部动态获取代理对象
    public void pointcut(){

    }
    /*
    * 定义通知方法：
    *   1.前置通知：在目标方法之前执行.
    *   2.后置通知：在目标方法之后执行.
    *   3.异常通知：在目标方法执行之后抛出异常时执行.
    *   4.最终通知：都要执行的通知
    *   5.环绕通知：在目标方法执行前后都要执行的通知
    *
    *   记录程序状态：
    *   1.目标对象的class(类型)/类路径
    *   2.目标对象的方法名
    *   3.目标对象的方法的参数信息
    *   4.获取目标对象方法的返回值
    *   5.获取目标对象执行报错的异常信息
    * */
    //@Before("pointcut()")
    public void before(JoinPoint joinPoint){
        //1.获取目标对象的类型
        Class<?> targetClass = joinPoint.getTarget().getClass();
        //2.获取目标对象的路径
        String path = joinPoint.getSignature().getDeclaringTypeName();
        //3.获取目标对象的方法名称
        String methodName = joinPoint.getSignature().getName();
        //4.获取方法参数
        Object[] args = joinPoint.getArgs();
        System.out.println("类型："+targetClass);
        System.out.println("类路径："+path);
        System.out.println("方法名称："+methodName);
        System.out.println("参数："+args);
        System.out.println(Arrays.toString(args));
        System.out.println("你好，我是前置通知");
    }
    //注意事项：如果是多个参数，joinPoint必须位于第一位(程序规则必须遵循)
    //@AfterReturning(value = "pointcut()" ,returning = "result")
    public void afterReturn(JoinPoint joinPoint,Object result){ //定义result 接收方法调用后的返回值
        //如果需要获取当前的方法信息，则可以通过joinPoint获取
        System.out.println("我是后置通知，获取方法的返回值："+result);
    }
    //后置通知与异常通知是互斥的，只能有一个
    //@AfterThrowing(value = "pointcut()",throwing ="exception")
    public void afterThrowing(JoinPoint joinPoint, Exception exception){
        //打印异常
        //exception.printStackTrace();
        //获取异常信息
        System.out.println("我是异常通知"+exception.getMessage());
    }

    /*
    * 知识点：
    *   1.如果切入点表达式只对当前通知有效.则可以按照如下方式编辑
    *   要求：打印注解的ID值,动态拦截Find注解，并且要获取Find注解中的参数Id
    *   难点知识：动态获取注解的对象！！！
    *   代码解释：
    *       1.@annotation(find) 拦截find变量名称对应类型的注解
    *       2.当匹配该注解之后将注解对象当做参数转递给find
    *       优势：可以一步到位获取注解的内容，避免了反射的代码
    * */
    //@Before("@annotation(find)")
    public void before2(Find find){
        System.out.println("ID的值为："+find.id());
    }
    //@After("pointcut()") //最终通知
    public void after(){
        System.out.println("你好我是最终通知");
    }
    /*
    * 环绕通知：
    *   1.特点：
    *       1.方法执行前后，通知都要执行
    *       2.环绕通知可以控制目标方法是否执行
    *       3.环绕通知必须添加返回值
    *   2.proceed()
    *       作用一：如果有下一个通知，则执行下一个通知
    *       作用二：如果没有下一个通知，则执行目标方法
    *   注意事项：ProceedingJoinPoint只支持环绕通知
    */
    @Around("pointcut()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        System.out.println("环绕通知一开始");
        //AOP实现时间的监控
        long startTime=System.currentTimeMillis();
        //检查是否有其他通知 有进入下一个通知 没有执行目标方法
        Object result = joinPoint.proceed();
        //Object proceed=joinPoint.proceed();
        System.out.println("环绕通知二结束");
        long endTime=System.currentTimeMillis();
        long time = endTime - startTime;
        System.out.println("方法执行的时间："+time+"毫秒");
        //return null;(没有执行目标方法返回空)
        return result;
    }
}
