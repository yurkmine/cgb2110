package com.jt.demo2;

import com.jt.demo2.UserService.UserService;
import com.jt.demo2.config.SpringConfig;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Spring_AOP {
    public static void main(String[] args) {
        ApplicationContext context=new AnnotationConfigApplicationContext(SpringConfig.class);
        //理论值：根据接口获取实现类对象 但是与切入点表达式匹配 为了后续拓展方便 为其动态创建代理对象
        UserService userService = context.getBean(UserService.class);
        //如果是实现类对象.则方法没有被扩展
        //如果是代理类对象.则方法被扩展 aop有效
        System.out.println(userService.getClass());//class com.sun.proxy.$Proxy19 动态获取的代理对象
        //System.out.println(userService);//打印输出：你好，我是前置通知 默认调用toString()方法 AOP拦截执行拓展方法 getClass()非普通方法所以不拦截
        userService.addUser();
        //userService.findCount();//测试带返回值的操作
    }
}
