package com.jt.demo;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**本类表示从Spring容器中，动态获取Dog对象*/
public class SpringGetDog {
    public static void main(String[] args) {
        //1.指定spring配置文件的路径
        String resource = "spring.xml";
        //2.启动spring容器
        ApplicationContext context =new ClassPathXmlApplicationContext(resource);
        //3.从容器中获取对象 类型是Object必须强制类型转换
        Dog dog1 =(Dog) context.getBean("dog");
        Dog dog2 = context.getBean(Dog.class);
        System.out.println(dog1);//打印的是地址
        System.out.println(dog2);
        //4.对象调用方法
        dog1.hello();
        //利用反射机制获取对象
        getDog();
    }
    /**
     * Spring实例化对象的核心原理-反射机制
     * 注意事项：反射的代码，必然会调用对象身上的无参构造方法
     * */
    public static void getDog(){
        try {
            Dog dog = (Dog) Class.forName("com.jt.demo.Dog").newInstance();
            dog.hello();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
