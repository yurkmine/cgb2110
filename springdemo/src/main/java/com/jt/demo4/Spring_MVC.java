package com.jt.demo4;

import com.jt.demo4.config.SpringConfig;
import com.jt.demo4.contral.UserContraller;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Spring_MVC {
    public static void main(String[] args) {
        ApplicationContext context=new AnnotationConfigApplicationContext(SpringConfig.class);
        UserContraller userContraller = context.getBean(UserContraller.class);
        userContraller.addUser();
    }

}
