package com.jt.demo4.contral;

import com.jt.demo4.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class UserContraller {
    @Autowired
    private UserService userService;
    public void addUser(){
        userService.addUser();
    }
}
