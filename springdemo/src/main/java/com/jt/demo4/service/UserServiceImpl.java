package com.jt.demo4.service;

import com.jt.demo4.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService{
    @Autowired
    private UserMapper userMapper;
    public void addUser(){
        userMapper.addUser();
    }
}
