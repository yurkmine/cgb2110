package com.jt.demo2;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class SpringAnno{
    /*
    * 测试Spring管理对象Bean的生命周期
    * 接口中没有提供close方法，需要使用实现类进行操作
    * */
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context=new AnnotationConfigApplicationContext(SpringCatConfig.class);
        Snake snake = context.getBean(Snake.class);
        snake.eat();
        //关闭容器
        context.close();
    }
/*    public static void main(String[] args) {
        //利用注解启动Spring容器
        ApplicationContext context =new AnnotationConfigApplicationContext(SpringCatConfig.class);
        //根据类型获取对象
        Cat cat1 = context.getBean(Cat.class);
        Cat cat2 = context.getBean(Cat.class);
        System.out.println(cat1);
        System.out.println(cat2);
        cat1.hello();
    }*/
}
