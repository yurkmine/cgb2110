package com.jt.demo2;

import org.springframework.context.annotation.*;

/**本类用于测试Spring注解*/
@Configuration //这个注解 用来标识当前类是个配置类 其实就是配置文件
@ComponentScan("com.jt.demo2") //根据指定的包路径扫描注解,扫描当前包及其子孙包
public class SpringCatConfig {
    /**
     * 方法要求：注解管理对象--自定义对象
     * 1.必须为公有的
     * 2.必须添加返回值，返回值的对象，就是容器管理的对象
     * 3.方法中的名称就是bean的Id
     * 4.方法必须使用@Bean的注解标识，Spring才会执行该方法 标识该对象交给Spring容器管理
     * */
    @Bean
    @Scope("prototype")//表示多例对象 默认懒加载，所以Lazy对多例对象不管理
    @Lazy               //开启懒加载 管理单例对象进行懒汉式加载
    //@Scope("singleton")//表示单例对象，默认就是单例 默认饿汉式加载
    public Cat cat(){
        return new Cat();
    }
    @Bean
    public Snake snake(){
        return new Snake();
    }
}
