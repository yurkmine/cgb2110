package com.jt.demo2;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class Snake {
    //1.无参构造方法
    public Snake(){
        System.out.println("蛇蛋诞生了！！！");
    }
    //2.初始化方法
    @PostConstruct //构造方法之后
    public void init(){
        System.out.println("蛇壳破了，四处溜达！！！");
    }
    //3.业务方法
    public void eat(){
        System.out.println("蛇，四处觅食！！！");
    }
    //4.销毁方法
    @PreDestroy //销毁之前
    public void dest(){
        System.out.println("一道名菜，姜辣蛇！！！");
    }
}
