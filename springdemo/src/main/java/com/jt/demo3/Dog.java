package com.jt.demo3;

import org.springframework.stereotype.Component;

@Component  //将该类交给Spring容器进行管理 key：类名首字母小写 value：Dog.Class 反射机制创建对象
public class Dog implements Pet{
    public Dog(){
        System.out.println("Dog的无参构造");
    }
    @Override
    public void hello(){
        System.out.println("小狗叫");
    }
}
