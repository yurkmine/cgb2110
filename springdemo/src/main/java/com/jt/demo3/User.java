package com.jt.demo3;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component //将User对象交给Spring容器管理
public class User {
    /*
    *注入：将Spring容器中的对象进行引用
    * @Autowired:可以将容器中对象进行注入
    *       1.按照类型注入 如果注入的类型是接口，则自动查找其实现类对象进行注入
    *           注意事项：一般Spring框架内部的接口都是单实现，特殊条件下可以多实现
    *       2.按照名称注入 @Autowired +@Qualifier("id")
    * */
    @Autowired
    @Qualifier("cat")//按照id进行注入
    private Pet pet;
    public void hello(){
        pet.hello();
    }
    public User(){
        System.out.println("User的无参构造");
    }
}
