package com.jt.demo3;

import org.springframework.stereotype.Component;

@Component
public class Cat implements Pet{
    @Override
    public void hello(){
        System.out.println("小猫");
    }
}
