package com.jt.demo5.mapper;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Repository;

@Repository
@PropertySource(value="classpath:/adduser.properties",encoding ="utf-8")
public class UserMapperImpl implements UserMapper{
    /*
    * @Value 注解得作用：为属性赋值
    *           需求：从Spring容器中动态数据获取
    * */
    @Value("${name}")
    private String name;
    @Override
    public void addUser(){
        System.out.println("新增用户:"+name);
    }
}
