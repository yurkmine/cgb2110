package com.jt.demo5;

import com.jt.demo5.config.SpringConfig;
import com.jt.demo5.mapper.UserMapper;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class SpringValue {
    public static void main(String[] args) {
        ApplicationContext context=new AnnotationConfigApplicationContext(SpringConfig.class);
        UserMapper bean = context.getBean(UserMapper.class);//用接口去接，往大了去接，方便以后更改实现类
        bean.addUser();
    }
}
