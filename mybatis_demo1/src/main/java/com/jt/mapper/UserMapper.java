package com.jt.mapper;

import com.jt.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Mapper //将接口交给Spring容器管理 Map<userMapper,JDK代理对象>
public interface UserMapper {

    //指定接口方法 查询demo_user的全部数据
    List<User> findAll();

    User findUserById(int id);

    int saveUser(User user);

    int updateUser(User user);

    int deleteUser(int id);

    List<User> readUser(HashMap<String, Object> map);
    //如果参数是多值，则需要封装为单值Map
    List<User> readUser2(@Param("minAge") int minAge,@Param("maxAge") int maxAge);

    List<User> findByLike(String key);

    List<User> findInArray(Integer[] ids);

    List<User> findInMap(@Param("ids1") Integer[] ids1, @Param("ids2") Integer[] ids2);
}
