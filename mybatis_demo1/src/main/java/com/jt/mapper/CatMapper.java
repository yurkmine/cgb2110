package com.jt.mapper;

import com.jt.pojo.Cat;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface CatMapper {

    List<Cat> findCat(Cat cat);
}
