package com.jt.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.apache.ibatis.type.Alias;

import java.io.Serializable;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Alias("User") //注解标签  使用别名
//实现序列化接口 保证数据在多线程运行状态下的安全性
public class User implements Serializable {
    private Integer id;
    private String name;
    private Integer age;
    private String sex;
}
