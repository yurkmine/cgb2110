package com.jt.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class Emp implements Serializable {
    private Integer empId;
    private String empName;
    //一对一封装 封装部门对象获取部门对象属性完成一对一封装
    private Dept dept;//deptId deptName
    //private Integer deptId
}
