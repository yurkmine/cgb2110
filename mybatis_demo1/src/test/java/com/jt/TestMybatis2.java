package com.jt;

import com.jt.mapper.UserMapper2;
import com.jt.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class TestMybatis2 {
    @Autowired
    private UserMapper2 userMapper2;

    /*
    * 需求：根据用户不为null的属性充当where条件
    * 问题说明：前端数据传递时可能会有null数据 如果数据为null则不允许充当where条件
    * 解决方案：动态Sql的实现
    * */
    @Test
    public void testSqlWhere(){
        User user=new User();
        user.setName("黑熊精").setAge(3000).setSex("男");
        List<User> userList=userMapper2.findSqlWhere(user);
        System.out.println(userList);

    }
    @Test
    public void testUpdateUser(){
        User user=new User();
        user.setId(1).setAge(5000);
        userMapper2.updateUser(user);
        System.out.println("更新成功");
    }
    /**
     * 如果name有值,则根据name查询.
     * 如果name没有值,则根据age查询.
     * 如果name/age都没有值,则根据sex查询
     */
    @Test
    public void testChoose(){
        User user =new User();
        user.setName("北极熊").setAge(5000);
        List<User> userList=userMapper2.findChoose(user);
        System.out.println(userList);
    }
}
