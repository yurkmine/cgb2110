package com.jt;

import com.jt.mapper.DeptMapper;
import com.jt.mapper.EmpMapper;
import com.jt.pojo.Dept;
import com.jt.pojo.Emp;
import org.junit.jupiter.api.Test;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class TestMybatis4 {
    @Autowired
    private DeptMapper deptMapper;//执行顺序 MapperScan在主启动类需要执行才能注入对象
    @Autowired
    private EmpMapper empMapper;
    @Test
    public void testEmp(){
        List<Emp> empList=empMapper.findAll();
        System.out.println(empList);
    }
    @Test
    public void testDept(){
        List<Dept> deptList=deptMapper.findAll();
        System.out.println(deptList);
    }
    /*
    * 测试Mybatis的注解形式
    * */
    @Test
    public void testAnno(){
        List<Dept> deptList=deptMapper.selectAll();
        System.out.println(deptList);
        Dept dept=new Dept();
        dept.setDeptName("公关部");
        deptMapper.saveDept(dept);
    }
}
