package com.jt;

import com.jt.mapper.CatMapper;
import com.jt.pojo.Cat;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class TestMybatis5 {
    @Autowired
    private CatMapper catMapper;
    @Test
    public void findCat(){
        Cat cat=new Cat();
        List<Cat> catList=catMapper.findCat(cat);
        System.out.println(catList);
    }
}
