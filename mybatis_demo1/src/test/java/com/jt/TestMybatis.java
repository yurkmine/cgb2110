package com.jt;

import com.jt.mapper.UserMapper;
import com.jt.pojo.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;
import java.util.List;

@SpringBootTest //该注解的作用在进行代码测试时启动Spring容器，动态获取对象
public class TestMybatis {
    @Autowired
    private UserMapper userMapper;
    @Test
    public void Test(){
        System.out.println(userMapper.getClass());//class com.sun.proxy.$Proxy67 JDK动态代理对象
        List<User> userList = userMapper.findAll();
        System.out.println(userList);
    }
    /*
    * 实现通过id查询数据
    * */
    @Test
    public void Test2(){
        int id=1;
        User user=userMapper.findUserById(id);
        System.out.println(user);
    }
    /*
    * 实现用户入库操作
    * */
    @Test
    public void saveUser(){
        User user=new User();
        user.setSex("男").setName("元旦快乐").setAge(2020);
        int rows=userMapper.saveUser(user);
        System.out.println("添加成功:影响:"+rows+"行");
    }
    @Test
    public void updateUser(){
        User user=new User();
        user.setName("过年啦").setSex("飞机");
        int rows=userMapper.updateUser(user);
        System.out.println("修改成功:影响:"+rows+"行");
    }
    @Test
    public void deleteUser(){
        int id=1;
        int rows=userMapper.deleteUser(id);
        System.out.println("删除成功:影响:"+rows+"行");
    }
    /*
    * 业务：查询age>18岁 and age<100的用户
    * 知识点：
    *      1.mybatis的参数可以是基本类型或者字符串.
    *      2.如果遇到多个参数,应该使用对象(POJO)进行封装.
    *      3.如果通过pojo封装不方便.则使用功能最为强大的Map进行封装
    *      4.Mybatis的接口方法中只允许传递单值
    * */
    //封装Map集合业务意图不够明显
    @Test
    public void readUser(){
        HashMap<String, Object> map = new HashMap<>();
        map.put("minAge", 18);
        map.put("maxAge", 100);
        List<User> list=userMapper.readUser(map);
        System.out.println(list.toString());
    }
    //第二种写法
    @Test
    public void readUser2(){
        int minAge=18;
        int maxAge=100;
        List<User> list=userMapper.readUser2(minAge,maxAge);
        System.out.println(list.toString());
    }
    /*
    * 需求：查询name中 包含“君”字的数据
    * Sql:where name like “%君%”
    * */
    @Test
    public void findByLike(){
        String key="君";
        List<User> userList=userMapper.findByLike(key);
        System.out.println(userList);
    }
    /*数组*/
    @Test
    public void findInArray(){
        Integer[] ids={1,2,3,4,5};
        List<User> userList=userMapper.findInArray(ids);
        System.out.println(userList);
    }
    /*Map集合*/
    @Test
    public void findInMap(){
        Integer[] ids1={1,2,3,4,5,6};
        Integer[] ids2={7,8,9,10,11,12};
        List<User> userList=userMapper.findInMap(ids1,ids2);
        System.out.println(userList);
    }
}
