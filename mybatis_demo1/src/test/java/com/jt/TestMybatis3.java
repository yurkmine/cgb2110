package com.jt;

import com.jt.mapper.DogMapper;
import com.jt.pojo.Dog;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class TestMybatis3 {
    @Autowired
    private DogMapper dogMapper;
    @Test
    public void findAll(){
        Dog dog=new Dog();
        List<Dog> dogList=dogMapper.findDog(dog);
        System.out.println(dogList);
    }
}
