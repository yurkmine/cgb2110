package com.jt.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jt.mapper.ItemDescMapper;
import com.jt.mapper.ItemMapper;
import com.jt.pojo.Item;
import com.jt.pojo.ItemDesc;
import com.jt.vo.ItemVO;
import com.jt.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.List;


@Service
public class ItemServiceImpl implements ItemService{
    @Autowired
    private ItemMapper itemMapper;
    @Autowired
    private ItemDescMapper itemDescMapper;
    @Autowired
    private FileService fileService;

    /*
    * 分页：
    *       1.Sql写法 特点：针对专门的数据库开发mysql，Oracle，DB2 写法不一样  JDBC直连最快
    *       2.MP写法 特点：可以自行的也换数据库，MP会自动更具数据库的类型生成Sql 相对速度慢一点
    * */
    @Override
    public PageResult getItemList(PageResult pageResult) {
        //1.用户查询的where条件
        QueryWrapper<Item> queryWrapper=new QueryWrapper<>();
        boolean flag= StringUtils.hasLength(pageResult.getQuery());
        queryWrapper.like(flag,"title",pageResult.getQuery());
        //2.page是MP分页对象 参数一：页数 参数二：条数
        //实现需要配置类文件
        Page<Item> page=
                new Page<>(pageResult.getPageNum(),pageResult.getPageSize());//2个参数
        //MP在内部自动的获取 总数和分页后的结果
        page=itemMapper.selectPage(page,queryWrapper);//4个参数 2+2操作
        //获取数据之后封装
        long total=page.getTotal();
        List<Item> rows=page.getRecords();
        //返回总数和分页后的结果 给到pageResult
        return pageResult.setTotal(total).setRows(rows);
    }

    @Override
    @Transactional
    public void updateItemStatus(Item item) {
        item.setStatus(item.getStatus());
        itemMapper.updateById(item);
    }

    @Override
    @Transactional
    public void deleteItemById(Integer id) {
        //根据id查询item表的数据
        Item item =itemMapper.selectById(id);
        //获取图片信息
        String[] images = item.getImages().split(",");
        for (String imagePath:images) {
            fileService.deleteFile(imagePath);
        }
        itemMapper.deleteById(id);
        itemDescMapper.deleteById(id);
    }

    @Override
    @Transactional
    public void updateItem(Item item) {
        item.setTitle(item.getTitle()).setSellPoint(item.getSellPoint()).setPrice(item.getPrice()).setNum(item.getNum());
        QueryWrapper<Item> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq("title", item.getTitle());
        itemMapper.update(item,queryWrapper);
    }

    @Override
    public Item findItemById(Integer id) {
        return itemMapper.selectById(id);
    }

    @Override
    public void saveItem(ItemVO itemVO) {
        //1.入库商品表
        Item item=itemVO.getItem();
        item.setStatus(true);
        //ID主键自增 入库之后才会有ID 要求id自动回显
        itemMapper.insert(item);
        //商品入库操作
        //主键自动回显 useGeneratedKeys="true" keyColumn="字段名" keyProperty="属性"
        ItemDesc itemDesc=itemVO.getItemDesc();
        itemDesc.setId(item.getId());//item.getId() 可能为null 入库才会有id存在 MP自动回显所以可以直接写 保证业务逻辑
        itemDescMapper.insert(itemVO.getItemDesc());
    }

}
