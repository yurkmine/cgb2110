package com.jt.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jt.mapper.ItemCatMapper;
import com.jt.pojo.ItemCat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class ItemCatServiceImpl implements ItemCatService{
    @Autowired
    private ItemCatMapper itemCatMapper;
    /*
    * Map<Integer,List<ItemCat>>
    *       0       一级菜单集合
    *       1       二级菜单集合
    *       2       三级菜单集合
    *       3          null
    * 业务思路：判断是否已经存在parentId
    *           不存在：我是第一个元素 封装到list中 之后保存到map中
    *           存在： 获取指定map中的list集合 将自己追加到其中
    * 拓展知识：map每次都需要查询数据库，如果采用redis缓存服务器，则性能得到了极大的提升
    * */
    public Map<Integer,List<ItemCat>> getMap(){
        //1.查询所有数据
        List<ItemCat> list = itemCatMapper.selectList(null);
        //2.将list集合数据，封装到Map集合当中
        Map<Integer,List<ItemCat>> map=new HashMap<>();
        //遍历所有数据
        for (ItemCat itemCat:list) {
            int parentId=itemCat.getParentId();
            if(map.containsKey(parentId)){//key存在将数据追加 map.get 返回所映射的list集合
                map.get(parentId).add(itemCat);
            }else{//key不存在 我是当前parentId下的第一个子级
                List<ItemCat> tempList=new ArrayList<>();
                tempList.add(itemCat);
                map.put(parentId, tempList);
            }
            //容易出现parentId重复的情况
            /*//临时
            List<ItemCat> tempList=new ArrayList<>();
            tempList.add(itemCat);
            map.put(parentId,tempList);*/
        }
        return map;
    }
    //要求20毫秒 实现查询 查询一次数据库 代码的魅力
    //Map<parentId,List<ItemCat>子级>
    @Override
    public List<ItemCat> findItemCatList(Integer level){
        //Map集合就相当于数据库 在内存中 速度快
        Map<Integer, List<ItemCat>> map = getMap();
        if(level==1){
            return map.get(0);
        }
        if(level==2){//查询一级和二级
            return getTwoList(map);
        }
        return getThreeList(map);
    }

    @Override
    @Transactional
    public void updateStatus(ItemCat itemCat) {
        boolean status=itemCat.getStatus();
        itemCat.setUpdated(new Date());
        QueryWrapper queryWrapper=new QueryWrapper();
        queryWrapper.eq("id", itemCat.getId());
        itemCatMapper.update(itemCat.setStatus(status), queryWrapper);
    }

    @Override
    @Transactional//事务控制
    public void updateItemCat(ItemCat itemCat) {
        String name=itemCat.getName();
        //itemCat.setUpdated(new Date());
        QueryWrapper queryWrapper=new QueryWrapper();
        queryWrapper.eq("id", itemCat.getId());
        itemCatMapper.update(itemCat.setName(name),queryWrapper);
    }

    @Override
    @Transactional
    public void saveItemCat(ItemCat itemCat) {
         itemCat.setStatus(true).setUpdated(new Date()).setCreated(new Date());
        itemCatMapper.insert(itemCat);
    }

    @Override
    @Transactional
    public void deleteItemCat(ItemCat itemCat) {
        if(itemCat.getLevel()==3){
            itemCatMapper.deleteById(itemCat);
        }
        if(itemCat.getLevel()==2){
            Integer parentId=itemCat.getId();
                QueryWrapper<ItemCat> queryWrapper=new QueryWrapper<>();
                queryWrapper.eq("parent_id", parentId)
                        .or().eq("id", itemCat.getId());//.or()或者
                itemCatMapper.delete(queryWrapper);
            itemCatMapper.deleteById(itemCat);
        }
        if(itemCat.getLevel()==1){
            int oneId=itemCat.getId();//获取一级id
            QueryWrapper<ItemCat> queryWrapper=new QueryWrapper<>();
            queryWrapper.eq("parent_id", oneId);
            //获取当前一级id下的所有二级id 获取的二级id存在集合里面
            List twoIds = itemCatMapper.selectObjs(queryWrapper);
            //通过二级id删除以下所有三级数据
            queryWrapper.clear();
            //获取二级所有id存在集合 作为范围 用in
            queryWrapper.in(twoIds!=null&&twoIds.size()>0,"parent_id", twoIds)
                    .or().in(twoIds!=null&&twoIds.size()>0,"id", twoIds)
                    .or().eq("id", oneId);
            itemCatMapper.delete(queryWrapper);
        }
    }


    private List<ItemCat> getThreeList(Map<Integer, List<ItemCat>> map) {
        //1.获取一级和二级
        List<ItemCat> oneList = getTwoList(map);//oneList children有二级集合数据 一级数据不可能为空
        //2.遍历一级集合获取二级数据
        for (ItemCat oneItemCat:oneList) {
            List<ItemCat> twoList = oneItemCat.getChildren();
            if(twoList ==null ||twoList.size()==0){
            //二级数据可能为空 数据大小为0 表示数据没有二级 本次循环终止，开始下次循环 防止空指针异常
                continue;
            }
            //二级列表一定有值，可以查询三级列表
            for (ItemCat twoItemCat:twoList) {
                Integer parentId = twoItemCat.getId();
                List<ItemCat> threeList = map.get(parentId);
                //将三级保存到二级集合
                twoItemCat.setChildren(threeList);
                //没有后续操作三级可以为空 无需判断 没有后续业务
            }
        }
        return oneList; //oneList包含所有级别数据 一二三级数据
    }

    private List<ItemCat> getTwoList(Map<Integer, List<ItemCat>> map) {
        //1.获取一级菜单
        List<ItemCat> oneList=map.get(0);
        for (ItemCat oneItemCat:oneList) {
            int parentId=oneItemCat.getId();
            List<ItemCat> twoList = map.get(parentId);
            //2.将二级菜单封装到一级菜单children中
            oneItemCat.setChildren(twoList);
        }
        return oneList;
    }
    //性能太低了
    /*@Override
    public List<ItemCat> findItemCatList(Integer level) {
        QueryWrapper<ItemCat> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq("level", 1);
        List<ItemCat> oneList = itemCatMapper.selectList(queryWrapper);
        for (ItemCat oneItemCat:oneList) {
            queryWrapper.clear();
            queryWrapper.eq("parent_id", oneItemCat.getId());
            List<ItemCat> twoList = itemCatMapper.selectList(queryWrapper);
            oneItemCat.setChildren(twoList);
            for (ItemCat twoItemCat:twoList) {
                queryWrapper.clear();//情况历史条件
                queryWrapper.eq("parent_id",twoItemCat.getId());
                List<ItemCat> threeList = itemCatMapper.selectList(queryWrapper);
                twoItemCat.setChildren(threeList);
            }
        }
        return oneList;
    }*/
}
