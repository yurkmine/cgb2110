package com.jt.service;

import com.jt.mapper.RightsMapper;
import com.jt.pojo.Rights;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RightsServiceImpl implements RightsService{
    @Autowired
    private RightsMapper rightsMapper;

    /**
     * 难点：查询1-2级菜单数据，一级的对象中包含二级对象
     * 操作：
     *      1.简单程度：在业务层查询多次手动封装 效率低
     *      2.正常方式：利用Sql进行关联查询   效率高
     * */
    @Override
    public List<Rights> getRightsList() {
        //1.拆线呢一级菜单
        //2.循环遍历一级菜单
        //3.根据一级菜单查询二级菜单
        //4.封装二级到一级中
        return rightsMapper.getRightsList();
    }
}
