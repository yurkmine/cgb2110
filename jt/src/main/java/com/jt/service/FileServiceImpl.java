package com.jt.service;

import com.jt.vo.ImageVO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;


@Service
public class FileServiceImpl implements FileService{
    //定义根目录 windows系统采用该目录 linux系统需要切换目录
    //private String preFilePath="D:/Workspace/image";
    //编辑Linux路径
    private String preFilePath="/usr/local/images";
    //定义网络地址前缀
    private String preUrlPath="http://image.jt.com";
    /*
    * 业务思路:
    *       1:校验图片类型 jpg/png/gif
    *       2:校验是否为恶意程序 木马.exe.jpg
    *       3:将图片分目录存储  hash方式存储|时间格式存储 D:\Workspace\image
    *       4:防止图片重名,使用UUID.
    * */
    @Override
    public ImageVO upload(MultipartFile file) {
        //第一步：校验图片类型
        //1.获取文件名称 ex：abc.jpg
        //全转小写 java严格区分大小的 大写后缀也满足条件
        String fileName=file.getOriginalFilename().toLowerCase();
        if(!fileName.matches("^.+\\.(jpg|png|gif)$")){
            //如果图片不满足条件，则程序终止
            return null;
        }
        //第二步：校验是否为恶意程序
        try {
            BufferedImage bufferedImage = ImageIO.read(file.getInputStream());
            //获取宽高
            int height=bufferedImage.getHeight();
            int width=bufferedImage.getWidth();
            //判断宽高为0 一定不是图片
            if(height==0||width==0){
                return null;
            }
            //第三步：分目录存储 提高检索的效率 按照时间将目录划分 /yyyy/MM/dd/
            //将当前时间转为字符串拼接
            String datePath=new SimpleDateFormat("/yyyy/MM/dd/").format(new Date());
            //D:/Workspace/image/2022/01/10
            String fileDir=preFilePath+datePath;
            //创建目录
            File dirFile =new File(fileDir);
            //判断文件是否存在
            if(!dirFile.exists()){
                dirFile.mkdirs();//创建目录
            }
            //第四步：防止文件重名 获取UUID唯一的
            String uuid= UUID.randomUUID().toString().replace("-", "");
            int index=fileName.lastIndexOf(".");//判断.最后一次出现的位置
            String fileType=fileName.substring(index);//从.的下标开始向后截取获取数据 .+"后缀名"
            //新的文件名称
            fileName=uuid+fileType;
            //第五步：实现文件上传
            String filePath=fileDir+fileName;
            file.transferTo(new File(filePath));
            //第六步：封装ImageVO对象并返回数据
             String virtualPath=datePath+fileName; //动态变化的路径(虚拟目录) 全路径去除根目录
            //第七步：封装网络地址 自定义前缀+虚拟地址
             String urlPath=preUrlPath+virtualPath;  //网络地址
            System.out.println(urlPath);//记录
            return new ImageVO(virtualPath,urlPath,fileName);
        } catch (IOException e) {
            e.printStackTrace();
            return  null;
        }
    }

    @Override
    public void deleteFile(String virtualPath) {
        String filePath=preFilePath+virtualPath;
        File file=new File(filePath);
        if(file.exists()){
            //存在则删除
            file.delete();
        }
    }
}
