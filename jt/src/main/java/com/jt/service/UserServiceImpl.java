package com.jt.service;

import com.jt.mapper.UserMapper;
import com.jt.pojo.User;
import com.jt.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;

import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserMapper userMapper;

    @Override
    public List<User> findAll() {
        return userMapper.findAll();
    }
    /*
    * 业务：用户登录操作
    * 登录业务逻辑
    *     1.将用户的密码进行加密处理
    *     2.根据用户名和密码进行数据库查询
    *         查到了：用户名和密码正确 返回token密钥
    *         没查到：用户名和密码错误 返回 null
    * */
    @Override
    public String login(User user) {
        //1.将密码加密处理  MD5算法
        String password=user.getPassword();
        String md5Pass=DigestUtils.md5DigestAsHex(password.getBytes());
        user.setPassword(md5Pass);
        //2.根据用户名和密码查询数据库
        User userDB= userMapper.findUserByUP(user);
        if(userDB==null){
            //说明用户名或者密码错误
            return null;
        }
        //密钥：该密钥是用户登录的唯一的标识符，特点：独一无二
        String token=UUID.randomUUID().toString()
                .replace("-", "");//UUID特殊标识"-"替换成空串
        return token;
    }

    @Override
    public PageResult findList(PageResult pageResult) {
        //1.获取记录总数
        long total =userMapper.findCount();
        //2.获取分页结果
        int size=pageResult.getPageSize();//查询条数
        int start =(pageResult.getPageNum()-1)*size;//limit开始
        String query=pageResult.getQuery();
        List<User> userList=userMapper.findUserLimit(query,start,size);
        pageResult.setTotal(total).setRows(userList);
        return pageResult;
    }

    @Override
    @Transactional //事务控制
    public void updateStatus(User user) {
        //获取当前时间
        user.setUpdated(new Date());
        userMapper.updateStatus(user);
    }

    @Override
    @Transactional
    public void deleteUser(Integer id) {
        userMapper.deleteUser(id);
    }

    @Override
    @Transactional
    public void addUser(User user) {
        String password=user.getPassword();
        String md5Pass=DigestUtils.md5DigestAsHex(password.getBytes());
        user.setPassword(md5Pass).setStatus(true);
        user.setCreated(new Date()).setUpdated(new Date());
        userMapper.addUser(user);
    }

    @Override
    public User findUserById(Integer id) {
        return userMapper.findUserById(id);
    }

    @Override
    @Transactional
    public void updateUser(User user) {
        user.setUpdated(new Date());
        userMapper.updateUser(user);
    }
}
