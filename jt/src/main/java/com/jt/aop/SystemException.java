package com.jt.aop;

import com.jt.vo.SysResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
//Spring专门为了解决异常，开发了正对Controller层的注解
//注解的核心就是 Spring-AOP
@RestControllerAdvice
public class SystemException {
    /*
    * 拦截什么异常：运行时异常 RuntimeException
    * 返回值：SysResult.fail()---201
    * @ExceptionHandler:切入点表达式
    * */
    @ExceptionHandler(RuntimeException.class)//类似于切入点表达式 PointCut
    public SysResult fail(Exception e){
        e.printStackTrace();
        return SysResult.fail();
    }
}
