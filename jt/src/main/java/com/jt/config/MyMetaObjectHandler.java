package com.jt.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component//将对象交给Spring容器管理
public class MyMetaObjectHandler implements MetaObjectHandler {
    //MP自动填充配置类文件 新增时更新
    @Override
    public void insertFill(MetaObject metaObject) {
        Date date=new Date();
        this.setFieldValByName("created", date, metaObject);
        this.setFieldValByName("updated", date, metaObject);
    }
    //MP自动填充配置类文件 修改时更新
    @Override
    public void updateFill(MetaObject metaObject) {
        Date date=new Date();
        this.setFieldValByName("updated", date, metaObject);
    }
}
