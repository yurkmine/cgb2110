package com.jt.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jt.pojo.Item;
import com.jt.vo.PageResult;

import java.util.List;

public interface ItemMapper extends BaseMapper<Item> {
}
