package com.jt.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jt.pojo.User;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;
                            //继承时,必须添加泛型.该泛型必须与表关联
                            //MP提供了强大的单表CRUD操作. 多表操作自己写
public interface UserMapper extends BaseMapper<User> {

    List<User> findAll();

    User findUserByUP(User user);
    @Select("select count(1) from user")
    long findCount();

    List<User> findUserLimit(@Param("query") String query, @Param("start") int start, @Param("size") int size);
    @Update("update user set status=#{status},updated=#{updated} where id=#{id}")
    void updateStatus(User user);
    @Delete("delete from user where id=#{id}")
    void deleteUser(Integer id);

    void addUser(User user);
    @Select("select * from user where id=#{id}")
    User findUserById(Integer id);
    @Update("update user set email=#{email},phone=#{phone},updated=#{updated} where id=#{id}")
    void updateUser(User user);
}
