package com.jt.controller;

import com.jt.pojo.Rights;
import com.jt.service.RightsService;
import com.jt.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/rights")
public class RightsController {
    @Autowired
    private RightsService rightsService;
    /**
     * url地址：/rights/getRightsList
     * 参数：无参
     * 请求类型：get
     * 返回值：SysResult(List<Rights>)1-2级
     *
     */
    @GetMapping("/getRightsList")
    public SysResult getRightsList(){
        List<Rights> rightsList=rightsService.getRightsList();
        return  SysResult.success(rightsList);
    }
}
