package com.jt.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class PortController {
    //从Spring容器中获取数据
    @Value("${server.port}")
    private Integer port;
    @GetMapping("/getPort")
    public String getPort(){
        return "获取端口号:"+port;
    }
}
