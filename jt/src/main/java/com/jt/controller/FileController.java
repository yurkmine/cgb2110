package com.jt.controller;

import com.jt.service.FileService;
import com.jt.vo.ImageVO;
import com.jt.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

@RestController
@RequestMapping("/file")
@CrossOrigin
public class FileController {
    @Autowired
    private FileService fileService;
    /*
    * 请求参数：file:二进制的字节信息
    *
    * 返回值: SysResult对象(ImageVO)
    * */
    @PostMapping("/upload")
    public SysResult upload(MultipartFile file){//字节输入流对象 SpringMVC封装的高效字节流API
        ImageVO imageVO=fileService.upload(file);
        if(imageVO==null){
            return SysResult.fail();
        }
        return SysResult.success(imageVO);
    }
    //Demo
    /*@PostMapping("/upload")
    public SysResult upload(MultipartFile file){//字节输入流对象 SpringMVC封装的高效字节流API
        String path="D:/Workspace/image/2110.jpg";//linux系统只识别“/”习惯
        try {
            file.transferTo(new File(path));//图片的全路径
        } catch (IOException e) {
            e.printStackTrace();
        }
        return SysResult.success();
    }*/
    @DeleteMapping("/deleteFile")
    public SysResult deleteFile(String virtualPath){
        fileService.deleteFile(virtualPath);
        return SysResult.success();
    }
}
