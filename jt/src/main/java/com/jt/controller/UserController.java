package com.jt.controller;

import com.jt.pojo.User;
import com.jt.service.UserService;
import com.jt.vo.PageResult;
import com.jt.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;

@RestController
@RequestMapping("/user")
@CrossOrigin    //前后端进行跨域操作
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/findAll")
    public List<User> findAll(){

        return userService.findAll();
    }
    /*
    * 业务需求：实现用户登录操作
      URL地址说明：/user/login
      * 请求类型 Post
      参数 username/password json串
      返回值： SysResult对象(token密钥)
     */
    @PostMapping("/login")
    public SysResult login(@RequestBody User user){
        String token=userService.login(user);
        //判断token是否为空
        if(token==null){
            return SysResult.fail();//表示用户登陆失败
        }
            return SysResult.success(token);

    }
    @GetMapping("/list")
    public SysResult findList(PageResult pageResult){//接收前端3条数据
        pageResult=userService.findList(pageResult);//3+2 接收前端3条数据 并且获取总数和分页查询结果
        return  SysResult.success(pageResult);
    }
    @PutMapping("/status/{id}/{status}")
    public SysResult updateStatus(User user){
        userService.updateStatus(user);
        return SysResult.success();
    }
    @DeleteMapping("/{id}")
    public SysResult deleteUser(@PathVariable Integer id){
        userService.deleteUser(id);
        return SysResult.success();
    }
    @PostMapping("/addUser")
    public SysResult addUser(@RequestBody User user){
        userService.addUser(user);
        return SysResult.success();
    }
    @GetMapping("/{id}")
    public SysResult findUserById(@PathVariable Integer id){
        User user=userService.findUserById(id);
        if(user==null){
            return SysResult.fail();
        }
        return SysResult.success(user);
    }
    @PutMapping("/updateUser")
    public SysResult updateUser(@RequestBody User user){
        userService.updateUser(user);
        return SysResult.success();
    }
}
