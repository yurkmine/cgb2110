package com.jt.pojo;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author 刘昱江
 * 时间 2021/2/2
 */
@Data
@Accessors(chain = true)
@TableName("user")//Mybatis-plus 对象与表一一映射
public class User extends BasePojo{
    @TableId(type = IdType.AUTO)//主键自增
    //@TableField("username")//标识属性与字段映射 如果属性名称与字段一致则省略
    private Integer id;
    private String username;
    private String password;
    private String phone;
    private String email;
    private Boolean status;     //true 正常 false 停用
}
