package com.jt.controller;


import com.jt.pojo.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;

@Controller
@ResponseBody //表示返回值统一为特定的字符串
public class UserController {
    @RequestMapping("/hello")
    public String hello(){
        return "您好，SpringMVC";
    }
    /*
    * 需求：拦截/say的请求： 要求返回值： 哈哈哈 元旦快乐
    * */
    @RequestMapping("/say")
    public String say(){
        return "哈哈哈哈 元旦快乐";
    }
    /*
    * 需求：根据ID 查询User数据
    * URL地址：http://localhost:8080/findUserById?id=value
    * 参数：id=value
    * 返回值：
    *   1.返回String类型  查询成功
    *   2.返回User对象
    * 知识点：
    *   1.SpringMVC为了简化取值过程 可以直接添加参数 源码如下
    *   2.SpringMVC中，请求的路径地址不能重复
    *   3.如果遇到多个参数，则可以使用对象的方式接收 对象的属性必须添加get/set方法
    *     SpringMVC如果发现我们的参数名称与对象的属性名称一致时则自动调用set方法完成赋值
    * */
    @RequestMapping("/findUserById")
    public String findUserById(int id){
        System.out.println("获取用户id："+id);
        return "查询成功";
    }
/*    public String findUserById(HttpServletRequest request){
        //SpringMVC整合Servlet之后的源码
        String id = request.getParameter("id");
        int intID = Integer.parseInt(id);
        System.out.println("获取用户的ID："+intID);
        return "查询成功";
    }*/

    /*
    * 多个参数传递问题
    * URL：http://localhost:8080/findUser?id=value&name=value&age=value
    * 参数：id=value&name=value&age=value
    * 返回值：String 查询成功
    * */
    @RequestMapping("/findUser")
    public String findUser(User user){
        System.out.println(user.toString());
        return "查询成功";
    }
    /*
    * 要求：返回User对象
    * URL：http://localhost:8080/getUserById?id=value
    * 返回值：User对象
    * 知识点：对象转化为json时，调用对象的get方法获取属性和属性值
    *       获取getXXX()方法，之后去除get 然后首字符小写 当做key
    *       方法的返回值当做value
    * */
    @RequestMapping("/getUserById")
    public User getUserById(int id){
        User user=new User();
        user.setId(id);
        user.setGender("女");
        user.setName("张老三");
        user.setAge(19);
        return user;
    }
    /*
    * SpringMVC动态参数接收 同名提交问题
    * 爱好 口 打篮球  口  踢足球  口 乒乓球
    * URL传递：http://localhost:8080/hobby?names=打篮球,踢足球,乒乓球
    * 返回值：返回爱好的list集合
    * 知识点：如果SpringMVC遇到了同名提交参数，并且中间使用”,“分隔 那么则可以自动转化为数组
    * */
    @RequestMapping("/hobby")
    public List<String> hobby(String names){
        /*//底层实现
        String[] array = names.split(",");
        //数组转集合
        List<String> strings = Arrays.asList(array);
        System.out.println(names);
        return strings;*/
        return Arrays.asList(names);
    }
    /*
    * 需求：使用RestFul结构接收参数
    * URL：http://localhost:8080/getUser/tomcat/18/男
    * 参数 name/age/gender
    * 返回值：User对象
    * 难点知识：如果获取URL路径中的参数!!
    * RestFul参数接收的语法
    *       1.参数使用{}进行包裹 并且指定属性名称
    *       2.使用注解进行get请求类型参数接收
    *       3.请求路径不能出现动词(ex:get/set)
    * */
    //当前路径只允许Get获取请求
    //@RequestMapping(value = "/user/{name}/{age}/{gender}",method = RequestMethod.GET)
    //简写
    @GetMapping("/user/{name}/{age}/{gender}")
    /*@PostMapping //Post请求
    @PutMapping //Put请求
    @DeleteMapping  //Delete请求*/
    public User getUser(User user){//get/set方法
        return user;
    }
    /*public User getUser(
                        @PathVariable String name,
                        @PathVariable int age,
                        @PathVariable String gender){
        User user=new User();
        user.setName(name);
        user.setAge(age);
        user.setGender(gender);
        return user;
    }*/

}
