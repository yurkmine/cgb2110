package com.jt.controller;

import com.jt.pojo.User;
import org.springframework.web.bind.annotation.*;

@RestController //@Controller + @ResponseBody
@CrossOrigin //允许跨域访问 可以传参数 只允许指定前端地址可以访问后端服务器
@RequestMapping("/axios") //将公共的部分抽取 前缀一般是业务名称
public class AxiosController {
    @RequestMapping("/findUser")
    public String findUser(){
        return "查询用户成功!!!";
    }
    /*
    * 需求：根据ID查询用户信息
    * URL：http://localhost:8080/axios/findUserById？id=100
    * 参数：id=100
    * 返回值User对象
    * */
    @RequestMapping("/findUserById")
    public User findUserById(int id){
        User user=new User();
        user.setId(id);
        user.setAge(18);
        user.setName("小芳");
        user.setGender("女");
        return user;
    }
    @RequestMapping("/findUserByNA")
    /*public User findUserByNA(String name,int age){
        User user=new User();
        user.setName(name);
        user.setAge(age);
        user.setId(10);
        user.setGender("女");
        return user;
    }*/
    public User findUserByNA(User user){
        user.setGender("男");
        user.setId(12);
        return user;
    }
    @RequestMapping("/user/{name}/{age}")
    public User userByNA(User user){
        return user;
    }
    /*
    * 业务说明：接收post请求
    * url地址：http://localhost:8080/axios/savaUser
    * 参数：JSON串 {"id":1,"name":"张三","age":18,"sex":"女"}
    * 返回值：User对象
    * 知识点：
    *   1.能否将json串转化成对象 @RequestBody
    *   1.能否将对象转化成json串 @ResponseBody
    * */
    @PostMapping("/saveUser")
    public User savaUser(@RequestBody User user){
        System.out.println(user.toString());
        return user;
    }
    /*
    * 业务：完成修改操作
    * 类型：put类型
    * URL地址："http://localhost:8080/axios/updateUser"
    * 参数：user的JSON串
    * 返回值：User返回对象
    * */
    @PutMapping("/updateUser")
    public User updateUser(@RequestBody User user){
        return user;
    }
    @PutMapping("/user/{id}/{name}/{age}/{gender}")
    public User user( User user){
        return user;
    }
}
