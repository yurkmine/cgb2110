package com.home.demo;

import com.home.pojo.User;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

@RestController
@CrossOrigin
@RequestMapping("/axios")
public class AxiosController {
    @RequestMapping("/GetUserById")
    public User getUser(int id){
        User user=new User();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            String url="jdbc:mysql://localhost:3306/cgb211001?CharacterEncoding=utf8";
            Connection c = DriverManager.getConnection(url, "root", "root");
            String sql="select*from user";
            PreparedStatement ps = c.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                int a = rs.getInt(1);
                String b = rs.getString(2);
                String d = rs.getString(3);
                int e = rs.getInt(4);
                if(a==id){
                    user.setId(a);
                    user.setAge(e);
                    user.setName(b);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return user;
    }
}
